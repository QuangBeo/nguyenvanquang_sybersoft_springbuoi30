import logo from './logo.svg';
import './App.css';
import Header from './Layout/Header';
import Banner from './Layout/Banner';
import Item from './Layout/Item';
import Footer from './Layout/Footer';

function App() {
  return (
    <div className="App">
      <Header/>
      <div className="container p-5">
        <Banner/>
        <Item/>
      </div>
      <Footer/>
    </div>
  );
}

export default App;
